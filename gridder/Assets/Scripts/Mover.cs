﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Tester {
	public class Mover : MonoBehaviour {
		public delegate void GameObjectHandler(Component go);
		public event GameObjectHandler OnMoved;
		public event GameObjectHandler OnClicked;
		// Start is called before the first frame update

		private Vector3 _lastPosition;
		public void Start() {
			_lastPosition = transform.position;
		}

		private bool _isDragging = false;

		public void OnMouseDown() {
			this._isDragging = true;
			this.OnClicked?.Invoke(this);
		}
		public void OnMouseDrag() {
			if (!_isDragging) {
				return;
			}
			var point = Camera.main.ScreenToWorldPoint(Input.mousePosition);
			if (isNewPoint(point)) {
				moveTo(point);
			}
		}

		private void moveTo(Vector3 point) {
			var pos = this.transform.position;
			point.y = pos.y;
			this.transform.position = _lastPosition = point;
			OnMoved?.Invoke(this);
		}

		public void OnMouseUp() {
			_isDragging = false;
		}

		private bool isNewPoint(Vector3 point) {
			float moved = Mathf.Abs(point.x - _lastPosition.x) + Mathf.Abs(point.z - _lastPosition.z);
			return moved > 0.2f;

		}

	}

}