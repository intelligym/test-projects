﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Tester {

	public struct Coordinate {
		public int col;
		public int row;
	}
	public interface IGridRect {
		int left { get; }
		int bottom { get; }
		int width { get; }
		int height { get; }

		int right { get; }

		int top { get; }
	}


	public class RectFinder<T> {

		public List<IGridRect> getRectsFromGrid(T[,] values, Func<T, bool> predicate) {
			List<GridRect> openRects = new List<GridRect>(),
				ret = new List<GridRect>();
			// 1. start at top left (0, 0)
			// 2. For each of the open rects, check if the next cell matches
			// 3. If it does, add to the rect
			// 4. If it does not - 
			// 5.	Seal the right side
			// 6.	Seal the bottom
			// 7.	Break off a rect left of column, sealed right
			// 8.	Break off a rect right of column, sealed right
			for (int col = 0, lastcol = values.GetUpperBound(0); col <= lastcol; ++col) {

				this.onLineEnded(openRects, ret);
				for (int row = 0, lastrow = values.GetUpperBound(1); row <= lastrow; ++row) {
					var match = predicate(values[col, row]);

					if (!this.applyTileToRects(col, row, openRects, ret, match)) {
						openRects.Add(new GridRect() { left = col, bottom = row, width = 1, height = 1 });
					}
				}
			}
			return ret.Cast<IGridRect>().ToList();
		}

		public struct RectFinderResults {
			public List<IGridRect> openRects;
			public List<IGridRect> closedRects;
			public int row;
		}

		public IEnumerator<RectFinderResults> enumerateRectsFromGrid(T[,] values, Func<T, bool> predicate) {
			List<GridRect> openRects = new List<GridRect>(),
				ret = new List<GridRect>();
			// 1. start at top left (0, 0)
			// 2. For each of the open rects, check if the next cell matches
			// 3. If it does, add to the rect
			// 4. If it does not - 
			// 5.	Seal the right side
			// 6.	Seal the bottom
			// 7.	Break off a rect left of column, sealed right
			// 8.	Break off a rect right of column, sealed right
			int lastrow = values.GetUpperBound(1);
			for (int row = 0; row <= lastrow; ++row) {

				this.onLineEnded(openRects, ret);
				for (int col = 0, lastcol = values.GetUpperBound(0); col <= lastcol; ++col) {
					var match = predicate(values[col, row]);

					if (!this.applyTileToRects(col, row, openRects, ret, match)) {
						if (match) {
							openRects.Add(new GridRect() { left = col, bottom = row, width = 1, height = 1 });
						}
					}
				}
				ret.RemoveAll(r => r.width < 2 || r.height < 2);
				if (row < lastrow) {
					yield return new RectFinderResults() {
						openRects = openRects.Cast<IGridRect>().ToList(),
						closedRects = ret.Cast<IGridRect>().ToList(),
						row = row
					};
				}
			}
			ret.AddRange(openRects.Where(r => r.width > 1 && r.height > 1));
			yield return new RectFinderResults() {
				openRects = new List<IGridRect>(),
				closedRects = ret.Cast<IGridRect>().ToList(),
				row = lastrow
			};
		}

		private bool addUniqueRect(GridRect rect, List<GridRect> to) {
			if (rect == null) {
				return false;
			}
			var container = to.FirstOrDefault(fc => fc.contains(rect));
			if (container != null) {
				return false;
			}
			var contained = to.Where(r => rect.contains(r)).ToArray();
			if (contained.Count() > 0) {
				foreach (var c in contained) {
					to.Remove(c);
				}
				to.Add(rect);
				return true;
			}
			to.Add(rect);
			return true;
		}

		private bool applyTileToRects(int col, int row, List<GridRect> openRects, List<GridRect> finalRects, bool match) {
			bool found = false;
			foreach (var rect in openRects.ToArray()) {
				if (col < rect.left || col > rect.right) {
					continue;
				}
				found = found || match;
				if (!match) {
					if (row > rect.bottom && col < rect.right) {
						var newRects = breakRectAt(rect, col, row);
						openRects.Remove(rect);
						newRects.ForEach(r => addUniqueRect(r, openRects));
						if (rect.height > 1) {
							addUniqueRect(rect, finalRects);
						}
					}
				}
				else if (rect.right == col && rect.height == 1) {
					rect.width++;
				}
				else if (rect.right == (col + 1)) {
					rect.height++;
				}
			}
			return found;
		}

		private List<GridRect> breakRectAt(GridRect rect, int col, int row) {
			List<GridRect> ret = new List<GridRect>(2);
			if (col > rect.left) {
				ret.Add(new GridRect() { left = rect.left, bottom = rect.bottom, width = col - rect.left, height = row - rect.bottom + 1 });
			}
			if (col < rect.right - 1) {
				ret.Add(new GridRect() { left = col + 1, bottom = rect.bottom, width = rect.right - col - 1, height = rect.height });
			}
			return ret;
		}

		private void onLineEnded(List<GridRect> openRects, List<GridRect> finalRects) {
			openRects.RemoveAll(r => r.width < 2); // have nothing to do with 1 width rects
		}



	}

	class GridRect : IGridRect {
		public int left { get; set; }
		public int bottom { get; set; }
		public int width { get; set; }
		public int height { get; set; }

		public int right => left + width;
		public int top => bottom + height;

		public bool contains(IGridRect other) {
			if (other == null) {
				return false;
			}
			return other.bottom >= this.bottom && other.left >= this.left
				&& other.right <= this.right && other.top <= this.top;
		}

		public override string ToString() {
			return $"GridRect {left}/{bottom}, {width}x{height}";
		}
	}
}
