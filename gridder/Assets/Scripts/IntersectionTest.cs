﻿using System;
using UnityEngine;

namespace Tester {
	using PositionCode = Int32;


	public delegate bool SamePointTest(float x0, float y0, float x1, float y1, float threshold);
	public class IntersectionTest {


		const int INSIDE = 0; // 0000
		const int LEFT = 1;   // 0001
		const int RIGHT = 2;  // 0010
		const int BOTTOM = 4; // 0100
		const int TOP = 8;    // 1000

		/// <summary>
		/// Min distance between numbers that qualifies them as the same
		/// </summary>
		const float PROXIMITY_THRESHOLD = 0.001f;

		public class IntersectOptions {
			public readonly float threshold;
			public readonly SamePointTest isSamePoint;

			public IntersectOptions(float? threshold = null, SamePointTest isSamePoint = null) {
				this.threshold = threshold == null ? PROXIMITY_THRESHOLD : threshold.Value;
				this.isSamePoint = isSamePoint == null ? IntersectionTest.isSamePoint : isSamePoint;
			}
		}
		


		public static bool lineIntersectsRect(Vector2 start, Vector2 end, Rect rect, IntersectOptions options = null) {
			float xmin = rect.xMin,
				xmax = rect.xMax,
				ymin = rect.yMin,
				ymax = rect.yMax;

			// Compute the bit code for a point (x, y) using the clip
			// bounded diagonally by (xmin, ymin), and (xmax, ymax)

			// ASSUME THAT xmax, xmin, ymax and ymin are global constants.

			// Cohen–Sutherland clipping algorithm clips a line from
			// P0 = (x0, y0) to P1 = (x1, y1) against a rectangle with 
			// diagonal from (xmin, ymin) to (xmax, ymax).
			float x0 = start.x,
				y0 = start.y,
				x1 = end.x,
				y1 = end.y;
			// compute outcodes for P0, P1, and whatever point lies outside the clip rectangle
			if (options == null) {
				options = new IntersectOptions();
			}
			while (true) {
				PositionCode outcode0 = ComputeOutCode(rect, x0, y0);
				PositionCode outcode1 = ComputeOutCode(rect, x1, y1);
				if ((outcode0 | outcode1) == 0) {
					// bitwise OR is 0: both points inside window; exit loop
					// return true unless points are roughly the same
					//return Math.Abs(x0 - x1) > 0.001f || Math.Abs(y0 - y1) > 0.001f;
					return !options.isSamePoint(x0, y0, x1, y1, options.threshold);
				}
				else if ((outcode0 & outcode1) != 0) {
					// bitwise AND is not 0: both points share an outside zone (LEFT, RIGHT, TOP,
					// or BOTTOM), so both must be outside window; exit loop (accept is false)
					return false;
				}

				// failed both tests, so calculate the line segment to clip
				// from an outside point to an intersection with clip edge
				float x = 0,
					y = 0;

				// At least one endpoint is outside the clip rectangle; pick it.
				PositionCode outcodeOut = outcode1 > outcode0 ? outcode1 : outcode0;

				// Now find the intersection point;
				// use formulas:
				//   slope = (y1 - y0) / (x1 - x0)
				//   x = x0 + (1 / slope) * (ym - y0), where ym is ymin or ymax
				//   y = y0 + slope * (xm - x0), where xm is xmin or xmax
				// No need to worry about divide-by-zero because, in each case, the
				// outcode bit being tested guarantees the denominator is non-zero
				if ((outcodeOut & TOP) != 0) {           // point is above the clip window
					x = x0 + (x1 - x0) * (ymax - y0) / (y1 - y0);
					y = ymax;
				}
				else if ((outcodeOut & BOTTOM) != 0) { // point is below the clip window
					x = x0 + (x1 - x0) * (ymin - y0) / (y1 - y0);
					y = ymin;
				}
				else if ((outcodeOut & RIGHT) != 0) {  // point is to the right of clip window
					y = y0 + (y1 - y0) * (xmax - x0) / (x1 - x0);
					x = xmax;
				}
				else if ((outcodeOut & LEFT) != 0) {   // point is to the left of clip window
					y = y0 + (y1 - y0) * (xmin - x0) / (x1 - x0);
					x = xmin;
				}

				// Now we move outside point to intersection point to clip
				// and get ready for next pass.
				if (outcodeOut == outcode0) {
					x0 = x;
					y0 = y;
				}
				else {
					x1 = x;
					y1 = y;
				}
			}
		}

		private static bool isSamePoint(float x0, float y0, float x1, float y1, float threshold) {
			return Math.Abs(x0 - x1) <= threshold && Math.Abs(y0 - y1) <= threshold;
		}

		private static PositionCode ComputeOutCode(Rect r, float x, float y) {
			PositionCode code;

			code = INSIDE;          // initialised as being inside of [[clip window]]

			if (x < r.xMin)           // to the left of clip window
				code |= LEFT;
			else if (x > r.xMax)      // to the right of clip window
				code |= RIGHT;
			if (y < r.yMin)           // below the clip window
				code |= BOTTOM;
			else if (y > r.yMax)      // above the clip window
				code |= TOP;

			return code;
		}

	}
}