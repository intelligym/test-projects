﻿using UnityEngine;
using System.Collections.Generic;

namespace Tester {

	[RequireComponent(typeof(MeshRenderer))]
	[RequireComponent(typeof(MeshFilter))]
	public class GridMesh : MonoBehaviour {
		public struct GridMeshParams {
			public float width;
			public float height;
			public uint rows;
			public uint columns;
			public Color? color;
		}

		public struct GridPoint {
			public int row;
			public int col;
			public override string ToString() {
				return $"Grid Point {this.col} x {this.row}";
			}

			public override bool Equals(object obj) {
				if (!(obj is GridPoint)) {
					return false;
				}
				var target = (GridPoint)obj;
				return (target.row == this.row && target.col == this.col);
			}
		}

		public GridMeshParams meshParams {
			get => this._meshParams;
			set {
				this._meshParams = value;
				redrawQuads();
			}
		}

		public Mesh mesh {
			get {
				return this.gameObject.GetComponent<MeshFilter>().mesh;
			}
		}

		public Color getDefaultColor() {
			return _meshParams.color ?? new Color(0.9f, 0.9f, 0.5f, 0.25f);

		}

		private GridMeshParams _meshParams;

		public Color getTileColor(GridPoint tile) {
			if (!this.isValidPoint(tile)) {
				return getDefaultColor();
			}

			var colors = this.mesh.colors32;
			var ind = (tile.row * this._meshParams.columns + tile.col) * 4;
			return colors[ind];
		}



		public void setTileColor(GridPoint tile, Color32 color) {
			if (!this.isValidPoint(tile)) {
				return;
			}

			var colors = this.mesh.colors32;
			var ind = (tile.row * this._meshParams.columns + tile.col) * 4;
			colors[ind] =
				colors[ind + 1] =
				colors[ind + 2] =
				colors[ind + 3] =
				color;
			this.mesh.colors32 = colors;
		}

		public bool isValidPoint(GridPoint point) {
			return point.row >= 0 && point.col >= 0
				&& point.row < this._meshParams.rows && point.col < this._meshParams.columns;
		}



		public GridPoint pointToTile(Vector3 point) {
			var p = this._meshParams;
			if (p.columns == 0 || p.rows == 0 || p.width <= 0 || p.height <= 0) {
				return new GridPoint() { row = -1, col = -1 };
			}
			float tileWidth = p.width / p.columns;
			float tileHeight = p.height / p.rows;

			//int indexCount = 0;
			float ystart = p.height * 0.5f;
			float xstart = -p.width * 0.5f;

			int row = (int)((ystart - point.z) / tileHeight);
			int col = (int)((point.x - xstart) / tileWidth);

			return new GridPoint() { row = row, col = col };
		}


		private void redrawQuads() {
			MeshFilter filter = gameObject.GetComponent<MeshFilter>();
			filter.mesh?.Clear();
			var p = this._meshParams;
			if (p.columns == 0 || p.rows == 0 || p.width <= 0 || p.height <= 0) {
				return;
			}
			float tileWidth = p.width / p.columns;
			float tileHeight = p.height / p.rows;
			var mesh = new Mesh();
			var verticies = new List<Vector3>();

			//int indexCount = 0;
			float ystart = p.height * 0.5f;
			float xstart = -p.width * 0.5f;

			for (int row = 0; row < p.rows; row++) {
				for (int col = 0; col < p.columns; col++) {
					Rect r = new Rect(xstart + col * tileWidth, ystart - row * tileHeight - tileHeight, tileWidth, tileHeight);
					r.size = new Vector2(r.width * 0.98f, r.height * 0.98f);
					verticies.AddRange(new Vector3[] {

						new Vector3(r.xMin, 0, r.yMax),
						new Vector3(r.xMax, 0, r.yMax),
						new Vector3(r.xMax, 0, r.yMin),
						new Vector3(r.xMin, 0, r.yMin)

						//new Vector3(xstart + col * tileWidth, 0, ystart - row * tileHeight),
						//new Vector3(xstart + (col + 1) * tileWidth, 0, ystart - row * tileHeight),
						//new Vector3(xstart + (col + 1) * tileWidth, 0, ystart - (row + 1) * tileHeight),
						//new Vector3(xstart + col * tileWidth, 0, ystart - (row + 1) * tileHeight)
					}); ; ;
					//indicies.Add(indexCount++);
					//indicies.Add(indexCount++);
					//indicies.Add(2 * i + 0);
					//indicies.Add(2 * i + 1);
				}
			}

			List<Color32> colors = new List<Color32>(verticies.Count);
			var indicies = new List<int>(verticies.Count);

			Color color = p.color != null ? p.color.Value : new Color(0.9f, 0.9f, 0.5f, 0.25f);
			Color32 c32 = (Color32)color;
			for (int i = 0, len = verticies.Count; i < len; i++) {
				indicies.Add(i);
				colors.Add(color);
			}

			mesh.vertices = verticies.ToArray();
			mesh.SetIndices(indicies.ToArray(), MeshTopology.Quads, 0);
			mesh.colors32 = colors.ToArray();
			filter.mesh = mesh;

			MeshRenderer meshRenderer = gameObject.GetComponent<MeshRenderer>();
			meshRenderer.material = new Material(Shader.Find("Sprites/Default"));
		}

		private Material createMaterial() {
			return Resources.Load<Material>("Materials/transparent");
			//var shader = Shader.Find("Standard");
			//var m = new Material(shader);
			//m.color = new Color(1, 1, 1, 0);
			//m.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.One);
			//m.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
			//m.SetInt("_ZWrite", 0);
			//m.DisableKeyword("_ALPHATEST_ON");
			//m.DisableKeyword("_ALPHABLEND_ON");
			//m.EnableKeyword("_ALPHAPREMULTIPLY_ON");
			//m.renderQueue = 3000;
			//return m;
		}
	}
}
