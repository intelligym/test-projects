﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Tester {
	public class utils {
		public static Bounds getMaxBounds(GameObject g) {
			var b = new Bounds(g.transform.position, Vector3.zero);
			foreach (Renderer r in g.GetComponentsInChildren<Renderer>()) {
				b.Encapsulate(r.bounds);
			}
			return b;
		}
	}
}
