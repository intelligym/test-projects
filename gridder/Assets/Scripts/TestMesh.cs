﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

namespace Tester {

	public class TestMesh : MonoBehaviour {
		public Button computeButton;
		public Button randomizeButton;

		private GridMesh _grid = null;

		// Start is called before the first frame update
		void Start() {
			updateGrid();
			randomizeButton.onClick.AddListener(onCompute);
			randomizeButton.onClick.AddListener(onRandomize);
		}

		// Update is called once per frame
		void Update() {
			//if (Input.GetMouseButtonUp(0)) {
			//	handleClick(Input.mousePosition);
			//}
		}

		private bool _isDragging = false;
		private Color _currentDragColor = Color.clear;
		private GridMesh.GridPoint _lastColoredPoint;
		public void OnMouseDown() {
			_isDragging = true;
			var pt = Input.mousePosition;
			var tile = this.getTileForScreenPoint(pt);
			var color = this._grid.getTileColor(tile);
			var defaultColor = this._grid.getDefaultColor();
			_currentDragColor = color.Equals(defaultColor) ? Color.green : defaultColor;
			handleClick(pt);
			//Debug.Log($"Mouse down at {pt.x}-{pt.y}");
			//var finder = new RectFinder();
			//var found = finder.getRectsFromGrid(_grid);
			//Debug.Log(found);
		}

		public void OnMouseUp() {
			_isDragging = false;
		}

		public void OnMouseDrag() {
			if (_isDragging) {
				handleClick(Input.mousePosition);
			}
		}

		private void onCompute() {
			Debug.Log($"Grid {_grid.meshParams.columns} x {_grid.meshParams.rows}");

		}

		private void onRandomize() {
			Debug.Log($"Grid {_grid.meshParams.columns} x {_grid.meshParams.rows}");
		}


		private void updateGrid() {
			_grid = this.gameObject.AddComponent<GridMesh>();
			_grid.meshParams = new GridMesh.GridMeshParams() {
				columns = 12,
				rows = 12,
				width = transform.localScale.x,
				height = transform.localScale.y,
				color = new Color(0.9f, 0.5f, 0.5f, 0.1f)
			};
		}

		private void handleClick(Vector3 pt) {
			GridMesh.GridPoint tile = this.getTileForScreenPoint(pt);
			if (!tile.Equals(this._lastColoredPoint)) {
				this._lastColoredPoint = tile;
				this._grid.setTileColor(tile, _currentDragColor);
				Debug.Log($"Set color of tile {tile.col}/{tile.row} to {_currentDragColor} (screen position {pt})");
			}
		}

		private GridMesh.GridPoint getTileForScreenPoint(Vector3 pt) {
			var worldPoint = Camera.main.ScreenToWorldPoint(pt);
			return this._grid.pointToTile(worldPoint);

		}


		private void colorComponent(GameObject go, Color color) {
		}
	}
}