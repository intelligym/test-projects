﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.InputSystem;

namespace Tester {

	public static class Extension {
		public static Vector3 InvertXZ(this Vector3 v, float y = 0) {
			if (v.sqrMagnitude == 0) {
				return v;
			}
			if (y == 0) {
				y = v.y;
			}
			float x = v.x == 0 ? 0 : 1f / v.x;
			float z = v.z == 0 ? 0 : 1f / v.z;
			return new Vector3(x, y, z);
		}
	}

	public class tester : MonoBehaviour {

		struct GridPoint {
			public int col;
			public int row;
			public override string ToString() {
				return $"GridPoint {col}/{row}";
			}
		}

		delegate void GOHandler(GameObject go);

		public GameObject tileTemplate;
		public GameObject targetTemplate;
		public Button computeButton;
		public Button randomizeButton;



		public uint columns = 10;
		public uint rows = 10;
		public uint width;
		public uint height;

		enum TileColors {
			Normal, Path, Source, Target, Marked, OpenRect, FinalRect
		}

		private GameObject source, target;
		private Dictionary<TileColors, Color> colorMap = new Dictionary<TileColors, Color>() {
			{ TileColors.Normal, Color.green }, {TileColors.Marked, Color.red }, {TileColors.Path, Color.cyan },
			{ TileColors.Source, Color.blue }, { TileColors.Target, Color.magenta },
			{ TileColors.OpenRect, Color.Lerp(Color.red, Color.black, 0.1f) }, { TileColors.FinalRect, Color.Lerp(Color.red, Color.black, 0.3f)}
		};



		// Start is called before the first frame update
		void Start() {
			this._rows = (int)rows;
			this._cols = (int)columns;
			this.transform.localPosition = Vector3.zero;
			Mover mover;
			source = Instantiate(targetTemplate, this.transform, true);
			target = Instantiate(targetTemplate, this.transform, true);
			mover = source.AddComponent<Mover>();
			mover.OnMoved += onEndPointMoved;
			mover = target.AddComponent<Mover>();
			mover.OnMoved += onEndPointMoved;
			source.transform.position = new Vector3(0.5f, 1.5f, 0.5f);
			target.transform.position = new Vector3(2.5f, 1.5f, 2.5f);

			colorComponent(source, TileColors.Source);
			colorComponent(target, TileColors.Target);

			computeButton.onClick.AddListener(onCompute);
			randomizeButton.onClick.AddListener(onRandomize);

			updateGrid();
		}


		// Update is called once per frame
		void Update() {
			// so we can update the component in runtime
			this.numCols = (int)this.columns;
			this.numRows = (int)this.rows;
			//if (Input.GetKeyDown(KeyCode.Space)) {
			//	CollectRects();
			//}

			//if (Input.GetMouseButtonUp(0)) {
			//	handleClick(Input.mousePosition);
			//}
		}

		//private void OnMouseDown() {
		//	var gridPoint = this.screenPointToGridPoint(Input.mousePosition);
		//	if (isValidCoordinate(gridPoint.col, gridPoint.row)) {
		//		var color = this._gridValues[gridPoint.col, gridPoint.row];
		//		setTileColor(gridPoint.col, gridPoint.row, color == TileColors.Normal ? TileColors.Marked : TileColors.Normal);
		//	}
		//}

		public void OnComputeNext() {
			collectRects();
		}

		public void OnMark(InputValue input) {
			var screenPoint = Mouse.current.position;
			var xy = new Vector2(x: screenPoint.x.ReadValue(), y: screenPoint.y.ReadValue());
			var worldPt = Camera.main.ScreenToWorldPoint(xy);
			Debug.Log($"Click in {xy.x}/{xy.y} world: ${worldPt.x}/{worldPt.z}/{worldPt.z}");
			var gridPoint = this.screenPointToGridPoint(xy); // this.screenPointToGridPoint(Input.mousePosition);
			if (isValidCoordinate(gridPoint.col, gridPoint.row)) {
				var color = this._gridValues[gridPoint.col, gridPoint.row];
				setTileColor(gridPoint.col, gridPoint.row, color == TileColors.Normal ? TileColors.Marked : TileColors.Normal);
			}
		}

		private void onEndPointMoved(Component go) {
			recalcPath();
		}

		private Color getColorForValue(TileColors color) {
			Color ret;
			if (!colorMap.TryGetValue(color, out ret)) {
				Debug.LogWarning($"Unknown color value {color}");
				ret = this.colorMap[TileColors.Normal];
			}
			return ret;
		}

		private void setTileColor(int col, int row, TileColors value) {
			var tile = this.getTile(col, row, out TileColors color);
			if (tile) {
				this._gridValues[col, row] = value;
				this.colorComponent(tile, value);
			}
		}


		private void recalcPath() {
			forAllTiles(go => colorComponent(go, TileColors.Normal));
			var scale = transform.localScale;
			var inverseScale = new Vector3(1f / scale.x, 1f / scale.y, 1f / scale.z);
			var srcPosition = source.transform.position;
			var trgPosition = target.transform.position;
			srcPosition.Scale(inverseScale);
			trgPosition.Scale(inverseScale);
			var src = gridPositionFromPoint(srcPosition);
			var trg = gridPositionFromPoint(trgPosition);

			if (src != null && trg != null) {
				drawPath(src.Value, trg.Value);
				GameObject srcTile = getTile(src.Value.col, src.Value.row, out _),
					trgTile = getTile(trg.Value.col, trg.Value.row, out _);
				colorComponent(srcTile, TileColors.Path);
				colorComponent(trgTile, TileColors.Path);
				Vector3 pos1 = srcTile.transform.position,
					pos2 = trgTile.transform.position;
				pos1.y = pos2.y = 2f;
				Debug.DrawLine(pos1, pos2, Color.red, 10f);
			}
		}

		private float tileWidth => (float)width / numCols;

		private float tileHeight => (float)height / numRows;

		private int _rows = 0,
			_cols = 0;
		private int numRows {
			get {
				return this._rows;
			}
			set {
				if (value != this._rows) {
					this._rows = value;
					updateGrid();
				}
			}
		}

		private int numCols {
			get {
				return this._cols;
			}
			set {
				if (value != this._cols) {
					this._cols = value;
					updateGrid();
				}
			}
		}

		private GridPoint? gridPositionFromPoint(Vector3 worldPoint) {
			worldPoint.x += width * 0.5f;
			worldPoint.z += height * 0.5f;
			int x = (int)Math.Floor(worldPoint.x);
			int y = (int)Math.Floor(worldPoint.z);

			return isValidCoordinate(x, y) ? new GridPoint?(new GridPoint() { col = x, row = y }) : null;
		}

		private void updateGrid() {
			forAllTiles(go => Destroy(go));
			transform.localScale = new Vector3(1, 1, 1);
			this._gridValues = new TileColors[numCols, numRows];

			this._tiles = new GameObject[numCols, numRows];

			float baseX = -0.5f * this.numRows + 0.5f;
			float baseY = -0.5f * this.numCols + 0.5f;
			for (int x = this.numCols - 1; x >= 0; --x) {
				for (int y = this.numRows - 1; y >= 0; --y) {
					createTile(x, y);
				}
			}
			resize();
		}

		private void resize() {
			var bounds = utils.getMaxBounds(this.gameObject);
			var scale = transform.localScale;
			scale.x = this.width / bounds.size.x;
			scale.z = this.height / bounds.size.z;
			transform.localScale = scale;
			BoxCollider collider = this.GetComponent<BoxCollider>();
			collider.size = new Vector3(bounds.size.x, 1, bounds.size.z);
		}

		private void showRowLine(int row) {
			float hwidth = width / 2f;
			float hz = height / 2f;
			float rowHeight = (float)height / (float)numRows;
			var p1 = new Vector3(-hwidth, 1, row * rowHeight - hz + 0.5f * rowHeight);
			var p2 = new Vector3(hwidth, 1, row * rowHeight - hz + 0.5f * rowHeight);
			Debug.Log($"drawing line from {p1.x},{p1.z} to {p2.x},{p2.z}");
			Debug.DrawLine(p1, p2, Color.blue, 222);
		}


		private GameObject[,] _tiles;
		private TileColors[,] _gridValues;

		private GridPoint screenPointToGridPoint(Vector3 screenPt) {
			var raw = Camera.main.ScreenToWorldPoint(screenPt);
			var scaled = raw;
			//scaled.Scale(transform.localScale.InvertXZ(1));
			var pt = scaled;
			pt.Set(newX: pt.x + width * 0.5f, newY: pt.y, newZ: pt.z + height * 0.5f);
			int col = Mathf.FloorToInt(pt.x / tileWidth);
			int row = Mathf.FloorToInt(pt.z / tileHeight);
			return new GridPoint() { col = col, row = row };

		}

		private GameObject _createTile(Vector3 worldPoint) {
			var t = Instantiate(tileTemplate, transform);
			worldPoint.y = 0;
			t.transform.position = worldPoint;
			var scale = t.transform.localScale;
			scale.x = scale.z = 0.96f; // simulate border
			t.transform.localScale = scale;
			return t;
		}

		private void createTile(int col, int row) {
			var t = getTile(col, row, out _);
			if (t != null) {
				Destroy(t);
			}
			var worldPoint = new Vector3(this.numCols * -0.5f + col + 0.5f, 0, this.numRows * -0.5f + row + 0.5f);
			this._tiles[col, row] = _createTile(worldPoint);
			setTileColor(col, row, TileColors.Normal);
		}

		private bool isValidCoordinate(int col, int row) {
			return col >= 0 && row >= 0 && col < numCols && row <= numRows;
		}

		private GameObject getTile(int col, int row, out TileColors value) {
			if (isValidCoordinate(col, row)) {
				value = _gridValues[col, row];
				return _tiles[col, row];
			}
			else {
				value = 0;
				return null;
			}
		}

		private void colorComponent(GameObject go, TileColors color) {
			var rend = go.GetComponent<Renderer>();
			var mcolor = this.getColorForValue(color);
			rend.material.color = mcolor;
		}

		private void forAllTiles(GOHandler handler) {
			if (_tiles != null) {
				for (int col = 0, lastcol = _tiles.GetUpperBound(0); col <= lastcol; ++col) {
					for (int row = 0, lastrow = _tiles.GetUpperBound(1); row <= lastrow; ++row) {
						handler(_tiles[col, row]);
					}
				}
			}
		}

		private void mark(int col, int row) {
			GameObject t = getTile(col, row, out _);
			if (t != null) {
				colorComponent(t, TileColors.Marked);
			}
		}


		private Rect gridPointToRect(GridPoint point) {
			return gridPointToRect(point.col, point.row);
		}

		private Rect gridPointToRect(int col, int row) {
			Rect r = new Rect(this.numCols * -0.5f + col, (float)this.numRows * -0.5f + row, 1, 1);
			return r;
		}

		private GridPoint worldPointToGridPoint(Vector2 worldPoint) {
			GridPoint ret;
			ret.col = (int)Math.Floor(worldPoint.x / tileWidth + 0.5f * numCols * tileWidth);
			ret.row = (int)Math.Floor(worldPoint.y / tileHeight + 0.5f * numRows * tileHeight);
			return ret;
		}


		private GridPoint rectToGridPoint(Rect r) {
			return worldPointToGridPoint(r.center);
		}

		private void drawPath(GridPoint source, GridPoint target) {
			int x = source.col;
			int y = source.row;
			int targetX = target.col;
			int targetY = target.row;
			int dx, dy;
			int signX, signY;
			Vector2 point1 = gridPointToRect(source).center,
				point2 = gridPointToRect(target).center;
			//int n = 1 + dx + dy;


			List<Rect> rects = new List<Rect>(3);
			int count = Math.Max(numRows, numCols) * 2;
			do {
				mark(x, y);
				rects.Clear();
				dx = targetX - x;
				dy = targetY - y;
				signX = Math.Sign(dx);
				signY = Math.Sign(dy);
				if (signX != 0) {
					rects.Add(gridPointToRect(x + signX, y));
				}
				if (signY != 0) {
					rects.Add(gridPointToRect(x, y + signY));
					if (signX != 0) {
						rects.Add(gridPointToRect(x + signX, y + signY));
					}
				}
				bool found = false;
				IntersectionTest.IntersectOptions options = null;
				// Test with
				// new IntersectionTest.IntersectOptions(null, (float x0, float y0, float x1, float y1, float threshold) => false);
				foreach (var rect in rects) {
					if (found = IntersectionTest.lineIntersectsRect(point1, point2, rect, options)) {
						GridPoint targetTile = rectToGridPoint(rect);
						x = targetTile.col;
						y = targetTile.row;
						break;
					}
				}
				if (!found) {
					break; // should never happen, protect us from endless loop
				}
				if (--count <= 0) {
					Debug.LogError("endless loop");
					break;
				}

			} while (x != targetX || y != targetY);
		}

		private void onCompute() {
			Debug.Log("compute");
		}

		private void onRandomize() {
			var rand = new System.Random();
			for (int col = 0, nCols = numCols; col < nCols; ++col) {
				for (int row = 0, nRows = numRows; row < numRows; ++row) {
					setTileColor(col, row, rand.Next(0, 2) == 0 ? TileColors.Marked : TileColors.Normal);
				}
			}
			_rectFinder = null;
		}

		private void resetGridRects() {
			for (int col = 0; col < numCols; col++) {
				for (int row = 0; row < numRows; row++) {
					var value = this._gridValues[col, row];
					if (value == TileColors.FinalRect || value == TileColors.OpenRect) {
						setTileColor(col, row, TileColors.Marked);
					}
				}
			}
		}

		RectFinder<TileColors> _rectFinder = null;
		IEnumerator<RectFinder<TileColors>.RectFinderResults> rectFindStatus;
		private void collectRects() {
			Func<TileColors, bool> test = c => c >= TileColors.Marked;
			if (_rectFinder == null) {
				_rectFinder = new RectFinder<TileColors>();
				rectFindStatus = _rectFinder.enumerateRectsFromGrid(_gridValues, test);
			}
			if (rectFindStatus.MoveNext()) {
				var rects = rectFindStatus.Current;
				resetGridRects();
				applyColorToRects(rects.openRects, TileColors.OpenRect);
				applyColorToRects(rects.closedRects, TileColors.FinalRect);
				showRowLine(rects.row);
			}
			else {
				_rectFinder = null;
				rectFindStatus.Dispose();
				resetGridRects();
			}
		}

		private void applyColorToRects(List<IGridRect> rects, TileColors color) {
			foreach (var rect in rects) {
				for (int col = 0; col < rect.width; ++col) {
					for (int row = 0; row < rect.height; ++row) {
						setTileColor(rect.left + col, rect.bottom + row, color);
					}
				}
			}
		}

	}
}